# Comptages montants Transilien SNCF

App accessible ici : [site L'hypercube](https://lhypercube.arep.fr/comptages_transilien/)

Une app simple pour la visualistation des données de comptages de voyageurs montants sur le réseau Transilien - SNCF, issues de l'open-data sncf. 

![application](src/ensemble.png)

## Cartographie
Sur la gaudche de l'ecran, on trouve un affichage sous forme d'histogramme spatial
* Un menu déroulant permet de selectionner le type de jour: 
    * JOB : *Jour Ouvré de Base*,
    * Samedi,
    * Dimanche. 
* Un second menu permet de sélectionner une ligne en particulier. 

![gauche1](src/gauche_jours.png)
![gauche2](src/gauche_ligne.png)

## Analyses 
Sur la partie droite de l'ecran, on trouve des statistiques de base, réparties en trois onglets:
* Analyses globales :
    * Classement des lignes par fréquentation, 
    * Classement des gares par fréquentation. 
* Analyse par ligne :
    * Classement des gares sur la ligne sélectionnée, 
    * Histogramme de fréquentation journalier de la ligne.
* Analyse par gares :
    * Histogramme de fréquentaiton journalier pour toutes les lignes **d'une gare**

![droite1](src/droite_global.png)
![droite2](src/droite_ligne.png)
![droite3](src/droite_gare.png)



## Librairies utilisées
* app web : [`Dash`](https://dash.plotly.com/), 
* cartographie : [`pydeck`](https://deckgl.readthedocs.io/en/latest/), 
* traitement de données : [`pandas`](https://pandas.pydata.org/).


## Données brutes : open-data SNCF
* Comptages des voyageurs montants : [lien](https://ressources.data.sncf.com/explore/dataset/comptage-voyageurs-trains-transilien/information/)
* Codes couleurs des lignes : [lien](https://ressources.data.sncf.com/explore/dataset/codes-couleur-des-lignes-transilien/information/)
* Réferentiel des gares avec geolocalisation : [lien](https://datasncf.opendatasoft.com/explore/dataset/referentiel-gares-voyageurs/information/?disjunctive.gare_ug_libelle)

## Lancer l'application manuellement sur une VM Ubuntu
Le lancement se fait à l'aide de `waitress-serve`. Surement pas le plus propre, mais pour des petits déploiements, suffisant...

On peut choisir :
* Lancer en mode headless (pouvoir se déconnecter de la VM après ) : `nohup`,
* l'IP : `--host=0.0.0.0`,
* le port : `--port=8040`,
* conserver un fichier de log : `> log.waiter`.

La commande globale : `nohup waitress-serve --host=0.0.0.0 --port=8040 --threads=2  app:app.server > log.waiter &`

## Auteur
M BOGDAN - AREP L'hypercube.

## License
[License Ouverte 2.0 / Open License 2.0.](https://www.etalab.gouv.fr/licence-ouverte-open-licence/)
