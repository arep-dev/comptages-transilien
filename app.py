# -*- coding: utf-8 -*-
"""
Created on Thu Aug 10 14:23:42 2023

@author: bogdanm

AREP L'hypercube
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 17 17:40:11 2023

@author: bogdan
"""
import dash
from dash import html
from dash.dependencies import Output, Input, State
from dash import dcc
from dash import ctx
from dash import no_update
import dash_bootstrap_components as dbc
import dash_deck.DeckGL as dgl
import pydeck as pdk
import pandas as pd
import matplotlib.colors as colors
import matplotlib.cm as cmx
import locale
import plotly.graph_objects as go

import os
import json

# For proper alphabetical sorting in french
locale.setlocale(locale.LC_ALL, "fr_FR.UTF-8")

#### Mapbox token
mapbox_api_token = "./mapbox_token"

#### Create a colormap from AREP's colors
number_of_cmap_colors = 256
cmap_hc3=[
    [0.3529,0.4862,0.0431],
    [0.709803922,0.737254902,0.560784314],
    [0.992156863,0.952941176,0.57254902],
    [0.992156863,0.964705882,0.71372549],
    [1.0,1.0,1.0],
    [0.988, 0.823, 0.768],
    [0.96, 0.337, 0.329],
    [0.533, 0.129, 0.18],
]
hc3col = colors.LinearSegmentedColormap.from_list(
    "hc3col",
    cmap_hc3,
    N=number_of_cmap_colors
)

#### Initiate the Dash App
app = dash.Dash(
    __name__,
    external_stylesheets=[dbc.themes.DARKLY],
    suppress_callback_exceptions = True
)
app.title = "Fréquentation Gares Transilien"

tab_selected_style = {
    "borderTop": "1px solid #e04e51",
    "borderBottom": "0px solid #D6D6D6",
    "borderLeft": "1px solid #3c3c3b !important",
    "boderRight": "1px solid #3c3c3b !important",
    "backgroundColor": "#9d9d9c",
    "color": "#f8f8f8",
    "padding": "0px"
}

tab_style = {
    "borderTop": "0px solid #D71B1B",
    "borderBottom": "0px solid #D6D6D6",
    "borderLeft": "1px solid #3c3c3b !important",
    "boderRight": "1px solid #3c3c3b !important",
    "backgroundColor": "#3c3c3b",
    "color": "#f8f8f8",
    "padding": "6px"
}
# app._favicon = ("favicon.png")

#### Mapbox token
# mapbox_api_token = "./mapbox_token"

## Read and prepare the data
# Lecture fichier
raw_data = pd.read_csv('./data/comptage-voyageurs-trains-transilien.csv',sep=";")


color_code_lines = pd.read_csv('./data/codes-couleur-des-lignes-transilien.csv',sep=";")
# Modification de la colonne tranche horaire
raw_data.replace(
    {
         "Avant 6h": "0-6",
         "De 6h à 10h": "6-10",
         "De 10h à 16h": "10-16",
         "De 16h à 20h": "16-20",
         "Après 20h": "20-24"
    },
    inplace = True)

#
raw_loc = pd.read_csv('./data/sncf-gares-et-arrets-transilien-ile-de-france.csv',sep=";")
raw_loc = raw_loc[['Code UIC','Coord GPS (WGS84)']]
raw_loc[["lat","lng"]] = raw_loc['Coord GPS (WGS84)'].str.split(',',expand=True)

raw_loc["lat"] = raw_loc["lat"].astype(float)
raw_loc["lng"] = raw_loc["lng"].astype(float)


raw_loc.rename(columns = {'Code UIC':'Code Gare'}, inplace = True)
initial_data = pd.merge(raw_data,raw_loc,on="Code Gare", how='left')

lignes_uniques = initial_data.Ligne.unique()
gares_uniques = initial_data['Nom gare'].drop_duplicates().sort_values()
# initial_data[["lat","lng"]] = initial_data["WGS 84"].str.split(",",expand=True)
# initial_data['lat'] = initial_data['lat'].astype(float)
# initial_data['lng'] = initial_data['lng'].astype(float)

 
## Define the app layout
app.layout = html.Div(
    [
        dbc.Row([
            dbc.Col(
                [
                    
                    dbc.Card(
                        id='deck-card',
                        style={'height': '100vh','margin-left':'0px'},
                        body=True,
                        children=[
                            html.Div(
                                'Voyageurs montants sur le réseau Transilien / SNCF',
                                style={
                                    "color":"#E04E51",
                                    "position": "fixed",
                                    "z-index": "999",
                                    "font-size":"30px",
                                    "font-weight": "bold",
                                    "margin-left":"10px"
                                }),
                             dbc.Select(
                                options = [
                                    "JOB",
                                    "Samedi",
                                    "Dimanche",
                                ],
                                value = "JOB",
                                id = "dropdown_map_day",
                                style = {
                                    "width":"10em",
                                    "display":"inline",
                                    "margin-top":"50px",
                                    "position": "fixed",
                                    "z-index": "999",
                                    "margin-left":"10px"
                                },
                            ),
                            dbc.Select(
                                options = ["Tout le réseau"]+[elt for elt in lignes_uniques],
                                value = "Tout le réseau",
                                id = "dropdown_map_line",
                                style = {
                                    "width":"10em",
                                    "display":"inline",
                                    "margin-top":"50px",
                                    "position": "fixed",
                                    "z-index": "999",
                                    "margin-left":"175px"
                                },
                            ),
                            html.Div(
                                id='map-container',
                                children=[
                                ],
                            ),
                            html.Div(
                                [
                                    dbc.Button(
                                        "Infos",
                                        id="open",
                                        n_clicks=0,
                                        style={
                                             "color":"#e04e51",
                                             "z-index": "999",
                                             "font-size":"12px",
                                             "font-weight": "bold",
                                             "border-color":"#e04e51",
                                             "background-color":"#3c3c3b",
                                             "position":"absolute",
                                             "bottom":25
                                        }
                                    ),
                                    dbc.Modal(
                                        [
                                            dbc.ModalHeader(dbc.ModalTitle("Infos")),
                                            dbc.ModalBody(
                                                dcc.Markdown("""
                                                     Infos :
                                                     * Il ne s'agit que la partie SNCF, les donénes RATP ne sont pas intégrées. 
                                                     * *JOB* : Jour Ouvré de Base *(moyenne du lundi au vendredi)*.
                                                    
                                                     Open data SNCF :
                                                     * 🧮 Comptages Transilien : [lien](https://ressources.data.sncf.com/explore/dataset/comptage-voyageurs-trains-transilien/information/)
                                                     * 🌍 Localisation gares : reconstruite à partir des codes UIC [lien](https://datasncf.opendatasoft.com/explore/dataset/referentiel-gares-voyageurs/information/?disjunctive.gare_ug_libelle)
                                                     * 🎨 Code couleurs des lignes Transilien : [lien](https://ressources.data.sncf.com/explore/dataset/codes-couleur-des-lignes-transilien/information/)
                                                     """
                                                 ),
                                            ),
                                            dbc.ModalFooter(
                                            dbc.Button(
                                                    "Fermer", id="close", className="ms-auto", n_clicks=0,
                                                    style={
                                                         "font-weight": "bold",
                                                         "border-color":"#e04e51",
                                                         "background-color":"#3c3c3b",

                                                    }
                                                )
                                            ),
                                        ],
                                        id="modal",
                                        is_open=False,
                                    ),
                                ]
                            ),
                            html.Div(
                                "❤️ L'hypercube - 2023 - Last update 17.01.2024",
                                style={
                                    "color":"#E04E51",
                                    "z-index": "999",
                                    "font-size":"12px",
                                    "font-weight": "bold",
                                    "position":"absolute",
                                    "bottom":5

                                }
                            ),
                        ]
                    ),
                    # html.Div(deck_component)
                    # ,
                    # html.Pre(id="click-info-json-output",style={
                                    # "color":"#E04E51",
                                    # "z-index": "999",
                                    # "font-size":"12px",
                                    # "left":10,
                                    # "position":"absolute",
                                    # "bottom":500

                                # }),
                ],
                width=6,
                style = {
                    "margin-right":"0px",
                    "margin-left":"-5px",
                    "padding-right":"0px",
                    "padding-left":"0px",
                }
            ),
            dbc.Col([
                dcc.Tabs(
                    id="tabs",
                    children =[
                    dcc.Tab(
                        label='Analyse Globale',
                        className='custom-tab',
                        style=tab_style,
                        selected_style=tab_selected_style,
                        children=[
                            html.Div(
                                "Type de jour : ",
                                style={"display":"inline"}
                            ),
                            dbc.Select(
                                options = [
                                    "JOB",
                                    "Samedi",
                                    "Dimanche",
                                ],
                                value = "JOB",
                                id = "dropdown_global_day",
                                style = {
                                    "width":"10em",
                                    "display":"inline",
                                    "margin-top":"5px",
                                    "margin-bottom":"5px"
                                },
                            ),
                            dcc.Graph(
                                id = "global_ligne",
                                style={
                                    "height": "40vh"
                                }
                            ),
                            dcc.Graph(
                                id = "global_gare",
                                style={
                                    "height": "50vh"
                                }
                            ),
                    ]),
                    dcc.Tab(
                        label='Analyse par Lignes',
                        className='custom-tab',
                        style=tab_style,
                        selected_style=tab_selected_style,
                        children=[
                            html.Div(
                                "Type de jour : ",
                                style={"display":"inline"}
                            ),
                            dbc.Select(
                                options = [
                                    "JOB",
                                    "Samedi",
                                    "Dimanche",
                                ],
                                value = "JOB",
                                id = "dropdown_line_day",
                                style = {
                                    "width":"10em",
                                    "display":"inline",
                                    "margin-top":"5px",
                                    "margin-bottom":"5px"
                                },
                            ),
                            html.Div(
                                "  Ligne : ",
                                style={"display":"inline"}
                            ),
                            dbc.Select(
                                options = [elt for elt in lignes_uniques],
                                value = "A",
                                id = "dropdown_line_name",
                                style = {
                                    "width":"10em",
                                    "display":"inline",
                                    "margin-top":"5px",
                                    "margin-bottom":"5px"
                                },
                            ),
                            dcc.Graph(
                                id = "ligne_histo",
                                style={
                                    "height": "50vh"
                                }
                            ),
                            dcc.Graph(
                                id = "ligne_gares",
                                style={
                                    "height": "40vh"
                                }
                            ),
                        ]
                    ),
                    dcc.Tab(
                        label='Analyse par Gares',
                        className='custom-tab',
                        style=tab_style,
                        selected_style=tab_selected_style,
                        children=[
                            html.Div(
                                "Type de jour : ",
                                style={"display":"inline"}
                            ),
                            dbc.Select(
                                options = [
                                    "JOB",
                                    "Samedi",
                                    "Dimanche",
                                ],
                                value = "JOB",
                                id = "dropdown_gare_day",
                                style = {
                                    "width":"10em",
                                    "display":"inline",
                                    "margin-top":"5px",
                                    "margin-bottom":"5px"
                                },
                            ),
                            html.Div(
                                "  Gare : ",
                                style={"display":"inline"}
                            ),
                            dbc.Select(
                                options = [elt for elt in gares_uniques],
                                value = "PARIS NORD",
                                id = "dropdown_gare_name",
                                style = {
                                    "width":"10em",
                                    "display":"inline",
                                    "margin-top":"5px",
                                    "margin-bottom":"5px"
                                },
                            ),
                            dcc.Graph(id = "gare_histo", style={"height":"50vh"}),
                        ]
                    ),
                ])
            ],
            width=6,
            style = {
                "margin-right":"0px",
                "margin-left":"0px",
                "padding-right":"0px",
                "padding-left":"0px",
                "border-left":"1px solid #9d9d9c"
            })
        ],
        style = {
            "margin-right":"0px",
            "margin-left":"0px",
            "padding-right":"0px",
            "padding-left":"0px"
        })
    ],style={"background-color":"#111111"}
)

@app.callback(
    Output("modal", "is_open"),
    [Input("open", "n_clicks"), Input("close", "n_clicks")],
    [State("modal", "is_open")],
)
def toggle_modal(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open

@app.callback(
    [
         Output('global_ligne', 'figure'),
         Output('global_gare', 'figure')
    ],
    Input('dropdown_global_day', 'value'))

def make_global_graphs(value):
    type_jour = value # JOB / Samedi / Dimanche
    lines_sorted = True
    data = initial_data[initial_data["Type jour"]==type_jour]
    somme_jour = data["Montants"].sum()
    # Par ligne
    line_data = data.groupby("Ligne").sum()["Montants"]
    if lines_sorted:
        line_data = line_data.sort_values(ascending=False)
    def color(color, text):
        return f"<span style='color:{str(color)}'> {str(text)} </span>"
    ticktext = [color(color_code_lines[color_code_lines["Ligne"]==l]['Code hexadécimal'].values[0], l) for l in line_data.index]
    # Crétaion de la figure
    fig1 = go.Figure()

    fig1.add_trace(
        go.Scatter(
            x=line_data.index,
            y=line_data.values,
            mode='lines+markers',
            line={
                'color':'rgba(245, 86, 84, 1)',
                'width':2
            },
            name=type_jour,
            hovertemplate=
            "Ligne : %{x}<br>"+
            "Voyageurs montants : %{y}<extra></extra>",
            showlegend = False,

        )
    )

    fig1.update_layout(
        title="Classement des lignes Transilien pour un "+
            type_jour+"<br>Total : "+
            format(somme_jour, ",").replace(","," ")+
            " montants",
        template='plotly_dark',
        # plot_bgcolor= 'rgba(100, 10, 0, 1)',
        # paper_bgcolor= 'rgba(100, 10, 0, 1)',
        bargap=0.02,
        xaxis={
            'title': 'Lignes Transilien',
            'gridcolor':'#3c3c3b',
            'mirror':True,
            'ticks':'inside',
            'showline':True,
            'linecolor':'#3c3c3b',
            'linewidth':3,
            'tickfont':{'size':20},
            'tickmode':'array',
            'ticktext':ticktext,
            'tickvals':line_data.index
        },
        yaxis={
            'title':'Total voyageurs montants',
            'tickformat':",",
            'gridcolor':'#3c3c3b',
            'mirror':True,
            'ticks':'inside',
            'showline':True,
            'linecolor':'#3c3c3b',
            'linewidth':3,
        },
        separators="* .*",
        font_family="segoe ui, Helvetica, Arial",
        hoverlabel={
            'bgcolor':"#3c3c3b",
            'font_size':14,
        }
    )

    # fig.show("browser")
    # Classement par gare.
    y_log_scale = None ## 'log' / None
    data = initial_data[initial_data["Type jour"]==type_jour]
    somme_jour = data["Montants"].sum()
    # Par ligne
    train_station_data = data.groupby("Nom gare").sum()["Montants"]
    train_station_data = train_station_data.sort_values(ascending=False)
    # Crétaion de la figure
    fig2 = go.Figure()

    fig2.add_trace(
        go.Scatter(
            x=train_station_data.index,
            y=train_station_data.values,
            mode='lines+markers',
            line={
                'color':'rgba(245, 86, 84, 1)',
                'width':2
            },
            name=type_jour,
            hovertemplate=
            "Gare : %{x}<br>"+
            "Voyageurs montants : %{y}<extra></extra>",
            showlegend = False,

        )
    )

    fig2.update_layout(
        title="Classement des gares Transilien pour un "+
            type_jour+
            "<br>Total : "+
            format(somme_jour, ",").replace(","," ")+
            " montants",
        template='plotly_dark',
        # plot_bgcolor= 'rgba(100, 10, 0, 1)',
        # paper_bgcolor= 'rgba(100, 10, 0, 1)',
        xaxis={
            'title': 'Gares',
            'gridcolor':'#3c3c3b',
            'mirror':True,
            'ticks':'inside',
            'showline':True,
            'linecolor':'#3c3c3b',
            'linewidth':3,
            'tickangle':45,
            'tickfont':{'size':10}
        },
        yaxis={
            'title':'Total voyageurs montants',
            'tickformat':",",
            'gridcolor':'#3c3c3b',
            'mirror':True,
            'ticks':'inside',
            'showline':True,
            'linecolor':'#3c3c3b',
            'linewidth':3,
            'type':y_log_scale
        },
        separators="* .*",
        font_family="segoe ui, Helvetica, Arial",
        hoverlabel={
            'bgcolor':"#3c3c3b",
            'font_size':14,
        }
    )

    return fig1, fig2


@app.callback(
    [
         Output('ligne_histo', 'figure'),
         Output('ligne_gares', 'figure'),
    ],
    [
         Input('dropdown_line_day', 'value'),
         Input('dropdown_line_name', 'value')
    ]
)

def make_line_graphs(day, line_name):
    ligne = line_name

    ##
    type_jour = day # JOB / Samedi / Dimanche
    ##

    data = initial_data[initial_data["Ligne"]==ligne]
    data_1 = data[data["Type jour"]==type_jour]
    somme_jour = data_1["Montants"].sum()

    _tmp = color_code_lines[color_code_lines["Ligne"]==ligne]
    lineColor = ('rgba('+
        str(_tmp["Code RVB Rouge"].values[0])+","+
        str(_tmp["Code RVB Vert"].values[0])+","+
        str(_tmp["Code RVB Bleu"].values[0])+",1)"
        )

    markerColor = ('rgba('+
        str(_tmp["Code RVB Rouge"].values[0])+","+
        str(_tmp["Code RVB Vert"].values[0])+","+
        str(_tmp["Code RVB Bleu"].values[0])+",0.5)"
        )
    markerBorderColor = lineColor
    maxValue = data_1.groupby("Tranche horaire").sum()["Montants"].max()
    # Histogramme global
    fig1 = go.Figure()

    fig1.add_trace(
        go.Bar(
            x=data_1.groupby("Tranche horaire").sum().index,
            y=data_1.groupby("Tranche horaire").sum()["Montants"],
            marker_color=markerColor,
            marker_line=dict(width=2, color=markerBorderColor),
            name=type_jour,
            customdata=data_1["Ligne"],
            hovertemplate=
            "Ligne <b>%{customdata}</b><br>"
            "Tranche horaire : %{x}<br>"+
            "Voyageurs montants : %{y}<extra></extra>",
            showlegend = False,

        )
    )
    data_1_sum = data_1.groupby('Tranche horaire').sum()
    fig1.add_trace(go.Scatter(
        x=data_1_sum.index,
        y=data_1_sum['Montants'],
        text=[format(elem,",").replace(","," ") for elem in data_1_sum['Montants'].values],
        mode='text',
        textposition='top center',
        textfont=dict(
            size=12,
        ),
        showlegend=False,
        cliponaxis= False,
        )
    )


    fig1.update_layout(
        title="Somme Ligne "+ligne+" / "+type_jour+
        "<br>Total : "+
        format(somme_jour, ",").replace(","," ")+
        " montants",
        template='plotly_dark',
        # plot_bgcolor= 'rgba(100, 10, 0, 1)',
        # paper_bgcolor= 'rgba(100, 10, 0, 1)',
        bargap=0.02,
        xaxis={
            'categoryorder':'array',
            'categoryarray':['0-6','6-10','10-16','16-20','20-24'],
            'tickmode' : 'array',
            'tickvals' : ['0-6','6-10','10-16','16-20','20-24'],
            'ticktext' : ['Avant 6h', '6h à 10h', '10h à 16h', '16h à 20h', 'Après 20h'],
            'title': 'Heures de la journée',
            'gridcolor':'#3c3c3b',
            'mirror':True,
            'ticks':'inside',
            'showline':True,
            'linecolor':'#3c3c3b',
            'linewidth':3
        },
        yaxis={
            'title':'Voyageurs montants',
            'tickformat':",",
            'gridcolor':'#3c3c3b',
            'mirror':True,
            'ticks':'inside',
            'showline':True,
            'linecolor':'#3c3c3b',
            'linewidth':3,
            'range': [0,1.1*maxValue]

        },
        separators="* .*",
        font_family="segoe ui, Helvetica, Arial",
        hoverlabel={
            'bgcolor':"#3c3c3b",
            'font_size':14,
        }
    )

    # Classement des gares

    # Par ligne
    train_station_data = data_1.groupby("Nom gare").sum()["Montants"]
    train_station_data = train_station_data.sort_values(ascending=False)


    # Crétaion de la figure
    fig2 = go.Figure()

    fig2.add_trace(
        go.Scatter(
            x=train_station_data.index,
            y=train_station_data.values,
            mode='lines+markers',
            line={
                'color':lineColor,
                'width':2
            },
            name=type_jour,
            hovertemplate=
            "Gare : %{x}<br>"+
            "Voyageurs montants : %{y}<extra></extra>",
            showlegend = False,

        )
    )

    fig2.update_layout(
        title="Ligne "+ligne+" / "+
            type_jour+
            "<br>Total : "+
            format(somme_jour, ",").replace(","," ")+
            " montants",
        template='plotly_dark',
        # plot_bgcolor= 'rgba(100, 10, 0, 1)',
        # paper_bgcolor= 'rgba(100, 10, 0, 1)',
        xaxis={
            'title': 'Gares',
            'gridcolor':'#3c3c3b',
            'mirror':True,
            'ticks':'inside',
            'showline':True,
            'linecolor':'#3c3c3b',
            'linewidth':3,
            'tickangle':45,
            'tickfont':{'size':10}
        },
        yaxis={
            'title':'Total voyageurs montants',
            'tickformat':",",
            'gridcolor':'#3c3c3b',
            'mirror':True,
            'ticks':'inside',
            'showline':True,
            'linecolor':'#3c3c3b',
            'linewidth':3,
            'type':None
        },
        separators="* .*",
        font_family="segoe ui, Helvetica, Arial",
        hoverlabel={
            'bgcolor':"#3c3c3b",
            'font_size':14,
        }
    )

    return fig1, fig2


@app.callback(
         Output('gare_histo', 'figure'),
    [
         Input('dropdown_gare_day', 'value'),
         Input('dropdown_gare_name', 'value')
    ]
)
def make_gare_graph(jour,nom_gare):
    gare = nom_gare

    ##
    type_jour = jour # JOB / Samedi / Dimanche
    ##

    data = initial_data[initial_data["Nom gare"]==gare]
    data_1 = data[data["Type jour"]==type_jour]

    somme_jour = data_1["Montants"].sum()

    if len(data.Axe.unique()) == 1 :

        nom_ligne = data_1["Ligne"].unique()[0]
        _tmp = color_code_lines[color_code_lines["Ligne"]==nom_ligne]
        markerColor = ('rgba('+
            str(_tmp["Code RVB Rouge"].values[0])+","+
            str(_tmp["Code RVB Vert"].values[0])+","+
            str(_tmp["Code RVB Bleu"].values[0])+",0.5)"
            )
        markerBorderColor = ('rgba('+
            str(_tmp["Code RVB Rouge"].values[0])+","+
            str(_tmp["Code RVB Vert"].values[0])+","+
            str(_tmp["Code RVB Bleu"].values[0])+",1)"
            )

        # Crétaion de la figure
        fig = go.Figure()

        fig.add_trace(
            go.Bar(
                x=data_1.groupby("Tranche horaire").sum().index,
                y=data_1.groupby("Tranche horaire").sum()["Montants"],
                marker_color=markerColor,
                marker_line=dict(width=2, color=markerBorderColor),
                name=type_jour,
                customdata=data_1["Ligne"],
                hovertemplate=
                "Ligne <b>%{customdata}</b><br>"
                "Tranche horaire : %{x}<br>"+
                "Voyageurs montants : %{y}<extra></extra>",
                showlegend = False,

            )
        )
        data_1_sum = data_1.groupby('Tranche horaire').sum()
        fig.add_trace(go.Scatter(
            x=data_1_sum.index,
            y=data_1_sum['Montants'],
            text=[format(elem,",").replace(","," ") for elem in data_1_sum['Montants'].values],
            mode='text',
            textposition='top center',
            textfont=dict(
                size=12,
            ),
            showlegend=False,
            cliponaxis= False,
            )
        )
        fig.update_traces(cliponaxis=False)

        fig.update_layout(
            title=gare+" / Ligne "+nom_ligne+" / "+type_jour+
            "<br>Total : "+
            format(somme_jour, ",").replace(","," ")+
            " montants",
            template='plotly_dark',
            # plot_bgcolor= 'rgba(100, 10, 0, 1)',
            # paper_bgcolor= 'rgba(100, 10, 0, 1)',
            bargap=0.02,
            xaxis={
                'categoryorder':'array',
                'categoryarray':['0-6','6-10','10-16','16-20','20-24'],
                'tickmode' : 'array',
                'tickvals' : ['0-6','6-10','10-16','16-20','20-24'],
                'ticktext' : ['Avant 6h', '6h à 10h', '10h à 16h', '16h à 20h', 'Après 20h'],
                'title': 'Heures de la journée',
                'gridcolor':'#3c3c3b',
                'mirror':True,
                'ticks':'inside',
                'showline':True,
                'linecolor':'#3c3c3b',
                'linewidth':3
            },
            yaxis={
                'title':'Voyageurs montants',
                'tickformat':",",
                'gridcolor':'#3c3c3b',
                'mirror':True,
                'ticks':'inside',
                'showline':True,
                'linecolor':'#3c3c3b',
                'linewidth':3,
            },
            separators="* .*",
            font_family="segoe ui, Helvetica, Arial",
            hoverlabel={
                'bgcolor':"#3c3c3b",
                'font_size':14,
            }
        )
    else:
        nom_axes = data_1["Axe"].unique().tolist()
        nom_lignes = data_1["Ligne"].unique()
        # Crétaion de la figure
        fig = go.Figure()

        for line in sorted(nom_axes):
            if line[0]!="T":
                nom_ligne = line[0]
            else:
                nom_ligne = line
            data_2 = data_1[data_1["Axe"]==line]

            _tmp = color_code_lines[color_code_lines["Ligne"]==nom_ligne]
            markerColor = ('rgba('+
                str(_tmp["Code RVB Rouge"].values[0])+","+
                str(_tmp["Code RVB Vert"].values[0])+","+
                str(_tmp["Code RVB Bleu"].values[0])+",0.5)"
                )
            markerBorderColor = ('rgba('+
                str(_tmp["Code RVB Rouge"].values[0])+","+
                str(_tmp["Code RVB Vert"].values[0])+","+
                str(_tmp["Code RVB Bleu"].values[0])+",1)"
                )

            fig.add_trace(
                go.Bar(
                    x=data_2.groupby("Tranche horaire").sum().index,
                    y=data_2.groupby("Tranche horaire").sum()["Montants"],
                    marker_color=markerColor,
                    marker_line=dict(width=2, color=markerBorderColor),
                    name=type_jour,
                    customdata=data_2["Axe"],
                    hovertemplate=
                    "Ligne <b>%{customdata}</b><br>"
                    "Tranche horaire : %{x}<br>"+
                    "Voyageurs montants : %{y}<extra></extra>",
                    showlegend = False,

                )
            )

        data_1_sum = data_1.groupby('Tranche horaire').sum()
        fig.add_trace(
            go.Scatter(
                x=data_1_sum.index,
                y=data_1_sum['Montants'],
                text=[format(elem,",").replace(","," ") for elem in data_1_sum['Montants'].values],
                mode='text',
                textposition='top center',
                textfont=dict(
                    size=12,
                ),
                cliponaxis= False,
                showlegend=False
            )
        )
        fig.update_traces(cliponaxis=False)

        fig.update_layout(
            title=gare+" / Lignes "+' & '.join(nom_lignes)+
            " / "+type_jour+
            "<br>Total : "+
            format(somme_jour, ",").replace(","," ")+
            " montants",
            template='plotly_dark',
            barmode='stack',
            # plot_bgcolor= 'rgba(100, 10, 0, 1)',
            # paper_bgcolor= 'rgba(100, 10, 0, 1)',
            bargap=0.02,
            xaxis={
                'categoryorder':'array',
                'categoryarray':['0-6','6-10','10-16','16-20','20-24'],
                'tickmode' : 'array',
                'tickvals' : ['0-6','6-10','10-16','16-20','20-24'],
                'ticktext' : ['Avant 6h', '6h à 10h', '10h à 16h', '16h à 20h', 'Après 20h'],
                'title': 'Heures de la journée',
                'gridcolor':'#3c3c3b',
                'mirror':True,
                'ticks':'inside',
                'showline':True,
                'linecolor':'#3c3c3b',
                'linewidth':3
            },
            yaxis={
                'title':'Voyageurs montants',
                'tickformat':",",
                'gridcolor':'#3c3c3b',
                'mirror':True,
                'ticks':'inside',
                'showline':True,
                'linecolor':'#3c3c3b',
                'linewidth':3,
            },
            separators="* .*",
            font_family="segoe ui, Helvetica, Arial",
            hoverlabel={
                'bgcolor':"#3c3c3b",
                'font_size':14,
            }
        )
    return fig


@app.callback(
         Output('map-container', 'children'),
         [
            Input('dropdown_map_day', 'value'),
            Input('dropdown_map_line', 'value')
        ]
)

def make_map(value,line):
    datamap = initial_data[initial_data["Type jour"]==value]
    
    if line == "Tout le réseau":
        columnColor= "[245,86,84,175]"
    else:
        datamap = datamap[datamap["Ligne"]==line]
        _tmp = color_code_lines[color_code_lines["Ligne"]==line]
        columnColor = ('['+
            str(_tmp["Code RVB Rouge"].values[0])+","+
            str(_tmp["Code RVB Vert"].values[0])+","+
            str(_tmp["Code RVB Bleu"].values[0])+",250]"
            )
    datamap1 = datamap.groupby("Nom gare")["Montants"].sum()

    new_df = pd.DataFrame()
    new_df['lng']=datamap.drop_duplicates("Nom gare").sort_values("Nom gare")['lng']
    new_df['lat']=datamap.drop_duplicates("Nom gare").sort_values("Nom gare")['lat'].values
    new_df['Gare']=datamap.drop_duplicates("Nom gare").sort_values("Nom gare")['Nom gare'].values
    new_df["Montants"]=datamap.groupby("Nom gare")["Montants"].sum().values
    new_df["Lignes"]= datamap.groupby("Nom gare")["Ligne"].unique().values
    new_df['Lignes'] = new_df['Lignes'].agg(lambda x: ','.join(map(str, x)))
    new_df.dropna(inplace=True)

    
    INITIAL_VIEW_STATE = pdk.ViewState(
                latitude=48.8566, longitude=2.3521, zoom=8, max_zoom=16, pitch=30, bearing=0
            )

    tooltip = {
        "html": "Gare : <b>{Gare}</b><br />"
                +"Ligne(s) : {Lignes}<br/>"
                +"Type de jour : "+value+"<br/>"
                +"Voyageurs montants : {Montants}<br />",
        "style": {
            "borderRadius": "5 px"
            }
    }
    column_layer = pdk.Layer(
            "ColumnLayer",
            new_df,
            get_position=["lng","lat"],
            get_elevation="Montants",
            get_fill_color=columnColor,
            radius=1000,
            opacity = 1,
            auto_highlight=True,
            elevation_scale=1,
            pickable=True
        )
    r = pdk.Deck(
       layers = [column_layer],
       initial_view_state=INITIAL_VIEW_STATE,
       api_keys={"mapbox": mapbox_api_token},
       map_style="dark"
    )

    deck_component = (dgl(
        r.to_json(),
        id="deck-gl",
        tooltip=tooltip,
        enableEvents=True,
        mapboxKey=r.mapbox_key
    ))

    return deck_component



@app.callback(
    [
        Output("dropdown_global_day", "value"),
        Output("dropdown_line_day", "value"),
        Output("dropdown_gare_day", "value"),
        Output("dropdown_map_day", "value")
    ],
    [
        Input("dropdown_global_day", "value"),
        Input("dropdown_line_day", "value"),
        Input("dropdown_gare_day", "value"),
        Input("dropdown_map_day", "value")
    ],
    prevent_initial_call = True
)

def synchronize_dropdowns_days(value1,value2,value3,value4):
    trig = ctx.triggered_id
    if trig == "dropdown_global_day":
        return value1, value1, value1, value1
    elif trig == "dropdown_line_day":
        return value2, value2, value2, value2
    elif trig == "dropdown_gare_day":
        return value3, value3, value3, value3
    else:
        return value4, value4, value4, value4

@app.callback(
    [
        Output("dropdown_map_line", "value"),
        Output("dropdown_line_name", "value")
    ],
    [
        Input("dropdown_map_line", "value"),
        Input("dropdown_line_name", "value")
    ],
    prevent_initial_call = True
)

def synchronize_dropdowns_lines(value1,value2):
    trig = ctx.triggered_id
    if trig == "dropdown_map_line":
        if value1 != "Tout le réseau":
            return value1, value1
        else:
            return value1, value2
        
    else:
        return value2, value2




#### Run the app
if __name__ == "__main__":
    app.run_server(
        debug=True,
        host="0.0.0.0",
        port="8030"
    )
